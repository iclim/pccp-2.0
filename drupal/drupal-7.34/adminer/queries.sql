#log query to get entries per day
select strftime('%d-%m-%Y', logtime) date, type, count(*) entries 
from import_log
where invoker = "API Call"
group by date, type
order by date desc