<?php


//Bootstrap
define('DRUPAL_ROOT', getcwd());
$_SERVER['REMOTE_ADDR'] = "localhost"; // Necessary if running from command line
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

function createReverseLink($node_id, $target_node_id, $target_node_field){
    // This function will create a reverse link on the opposing node

    if ($target_node_field && $target_node_id):
        //load target node
        $node = node_load($target_node_id);
        // ($node);
        $obj = array();
        $obj['target_id'] = $target_node_id;
        $obj['target_type'] = 'node';

        $alrady_linked = false;

        if ( !is_array($node->field_related_projects_entity['und']) ):
            $node->field_related_projects_entity['und'] = array();
        else:
            //check if node is already linked
            foreach($node->field_related_projects_entity['und'] as $entlink):
                if ($entlink['target_id'] == $obj['target_id']):
                    $alrady_linked = true;
                endif;
            endforeach;
        endif;

        if (!$alrady_linked):
            array_push($node->field_related_projects_entity['und'], $obj);
        endif;

        // $node->$target_node_field['und'][0]['target_id'] = $node_id; 
        // $node->$target_node_field['und'][0]['target_type'] = 'node'; 

        $node->changed = strtotime($last_modified->date);          //set the modified date if desired
        node_save($node);
        print_r($node);
        return $node->nid;
        // if($node = node_submit($node)) { 
        //     $node->changed = strtotime($last_modified->date);          //set the modified date if desired
        //     node_save($node);
        //     return $node->nid;
        // }
    endif;
    return false;


}

function createContact($title, $ref_id=false, $ref_type='node'){
    print 'Create Contact: '.$title.'.....';
    // first - check if contact already exists
    //find entities
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
      ->propertyCondition('type', 'contact')
      ->propertyCondition('title', $title)
      ->propertyCondition('status', 1)
      ->range(0,1)
      ->execute();

      if (!empty($entities['node'])) {

        $node = node_load(array_shift(array_keys($entities['node'])));
        node_object_prepare($node);
        // print_r($node);
        if ($node->nid):
            $obj = array();
            $obj['target_id'] = $ref_id;
            $obj['target_type'] = 'node';

            $alrady_linked = false;

            if ( !is_array($node->field_related_projects_entity['und']) ):
                $node->field_related_projects_entity['und'] = array();
            else:
                //check if node is already linked
                foreach($node->field_related_projects_entity['und'] as $entlink):
                    if ($entlink['target_id'] == $obj['target_id']):
                        $alrady_linked = true;
                    endif;
                endforeach;
            endif;

            if (!$alrady_linked):
                array_push($node->field_related_projects_entity['und'], $obj);
            endif;

            // array_push($node->field_related_content_entity['und'], $obj);
            //$node->field_related_projects_entity['und'][] = $obj; 
            node_save($node);
            print "Contact Exists\n";
        else:
            return false;
        endif;
        return $node->nid;
      }

    print 'CREAT(NG NEW CONTACT'."\n";
    $node = new stdClass(); // x`Create a new node object
    $node->type = "contact"; 
    node_object_prepare($node); // Set some default values
    $node->language = LANGUAGE_NONE;
    $node->status = 1; //<published>1</published> $node->field_joomla_status[$node->language][0]['value'] = 1;
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 1; //: 2 = comments open, 1 = comments closed, 0 = comments hidden
    $node->date = date("Y-m-d H:i:s");  //"2015-09-19 15:12:04"; 
    $node->uid = 31; //import user

    $node->title = $title; 

    //related content
    if ($ref_id):
        print 'setting related projects for contact: '.$ref_id;
        $obj = array();
        $obj['target_id'] = $ref_id;
        $obj['target_type'] = 'node';
        // print_r($obj);
        // array_push($node->field_related_content_entity['und'], $obj);
        $alrady_linked = false;

        if ( !is_array($node->field_related_projects_entity['und']) ):
            $node->field_related_projects_entity['und'] = array();
        else:
            //check if node is already linked
            foreach($node->field_related_projects_entity['und'] as $entlink):
                if ($entlink['target_id'] == $obj['target_id']):
                    $alrady_linked = true;
                endif;
            endforeach;
        endif;

        if (!$alrady_linked):
            array_push($node->field_related_projects_entity['und'], $obj);
        endif;
        // $node->field_related_projects_entity['und'][] = $obj; 


        
    endif;



    //SAVE NODE
    if($node = node_submit($node)) { 
        $node->changed = strtotime($last_modified->date); //set the modified date if desired
        node_save($node);
        // print_r($node);
        print "Contact Created\n";
        return $node->nid;
    }

    return false;
    
}

function createCountry($title, $ref_id=false, $ref_type='node'){
    print 'Relate Country: '.$title.'.....';
    // first - check if contact already exists
    //find entities
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
      ->propertyCondition('type', 'country')
      ->propertyCondition('title', $title)
      ->propertyCondition('status', 1)
      ->range(0,1)
      ->execute();

      if (!empty($entities['node'])) {

        $node = node_load(array_shift(array_keys($entities['node'])));
        node_object_prepare($node);
        // print_r($node);
        if ($node->nid):
            $obj = array();
            $obj['target_id'] = $ref_id;
            $obj['target_type'] = 'node';

             $alrady_linked = false;

            if ( !is_array($node->field_related_projects_entity['und']) ):
                $node->field_related_projects_entity['und'] = array();
            else:
                //check if node is already linked
                foreach($node->field_related_projects_entity['und'] as $entlink):
                    if ($entlink['target_id'] == $obj['target_id']):
                        $alrady_linked = true;
                    endif;
                endforeach;
            endif;

            if (!$alrady_linked):
                array_push($node->field_related_projects_entity['und'], $obj);
            endif;

            // if ( !is_array($node->field_related_projects_entity['und']) ):
            //     $node->field_related_projects_entity['und'] = array();
            // endif;
            // array_push($node->field_related_projects_entity['und'], $obj);

            // array_push($node->field_related_content_entity['und'], $obj);
            //$node->field_related_projects_entity['und'][] = $obj; 
            node_save($node);
            print "Country Exists\n";
        else:
            return false;
        endif;
        return $node->nid;
      }

    print 'CREAT(NG NEW COUNTRY'."\n";
    $node = new stdClass(); // x`Create a new node object
    $node->type = "country"; 
    node_object_prepare($node); // Set some default values
    $node->language = LANGUAGE_NONE;
    $node->status = 1; //<published>1</published> $node->field_joomla_status[$node->language][0]['value'] = 1;
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 1; //: 2 = comments open, 1 = comments closed, 0 = comments hidden
    $node->date = date("Y-m-d H:i:s");  //"2015-09-19 15:12:04"; 
    $node->uid = 31; //import user

    $node->title = $title; 

    //related content
    if ($ref_id):
        print 'setting related projects for country: '.$ref_id;
        $obj = array();
        $obj['target_id'] = $ref_id;
        $obj['target_type'] = 'node';

         $alrady_linked = false;

        if ( !is_array($node->field_related_projects_entity['und']) ):
            $node->field_related_projects_entity['und'] = array();
        else:
            //check if node is already linked
            foreach($node->field_related_projects_entity['und'] as $entlink):
                if ($entlink['target_id'] == $obj['target_id']):
                    $alrady_linked = true;
                endif;
            endforeach;
        endif;

        if (!$alrady_linked):
            array_push($node->field_related_projects_entity['und'], $obj);
        endif;
        // print_r($obj);
        // array_push($node->field_related_content_entity['und'], $obj);
        // $node->field_related_projects_entity['und'][] = $obj; 
        
    endif;



    //SAVE NODE
    if($node = node_submit($node)) { 
        $node->changed = strtotime($last_modified->date); //set the modified date if desired
        node_save($node);
        // print_r($node);
        print "Country Created\n";
        return $node->nid;
    }

    return false;
    
}

function createFund($title, $ref_id=false, $ref_type='node'){
    print 'Relate Fund: '.$title.'.....';
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
      ->propertyCondition('type', 'fund')
      ->propertyCondition('title', $title)
      ->propertyCondition('status', 1)
      ->range(0,1)
      ->execute();


      if (!empty($entities['node'])) {

        $node = node_load(array_shift(array_keys($entities['node'])));
        node_object_prepare($node);
        if ($node->nid):
            $obj = array();
            $obj['target_id'] = $ref_id;
            $obj['target_type'] = 'node';
            // array_push($node->field_related_projects_entity['und'], $obj);
             $alrady_linked = false;

            if ( !is_array($node->field_related_projects_entity['und']) ):
                $node->field_related_projects_entity['und'] = array();
            else:
                //check if node is already linked
                foreach($node->field_related_projects_entity['und'] as $entlink):
                    if ($entlink['target_id'] == $obj['target_id']):
                        $alrady_linked = true;
                    endif;
                endforeach;
            endif;

            if (!$alrady_linked):
                array_push($node->field_related_projects_entity['und'], $obj);
            endif;
            // $node->field_related_projects_entity['und'][] = $obj; 
            node_save($node);
            print "Fund Exists\n";
        else:
            return false;
        endif;
        return $node->nid;
      }

    print 'Create New Fund'."\n";

    $node = new stdClass(); // x`Create a new node object
    $node->type = "fund"; 
    node_object_prepare($node); // Set some default values
    $node->language = LANGUAGE_NONE;
    $node->status = 1; //<published>1</published> $node->field_joomla_status[$node->language][0]['value'] = 1;
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 1; //: 2 = comments open, 1 = comments closed, 0 = comments hidden
    $node->date = date("Y-m-d H:i:s");  //"2015-09-19 15:12:04"; 
    $node->uid = 31; //import user

    $node->title = $title; 

    //related content
    if ($ref_id):
        //print 'setting related content: '.$ref_id;
        $obj = array();
        $obj['target_id'] = $ref_id;
        $obj['target_type'] = 'node';
        // array_push($node->field_related_projects_entity['und'], $obj);
         $alrady_linked = false;

            if ( !is_array($node->field_related_projects_entity['und']) ):
                $node->field_related_projects_entity['und'] = array();
            else:
                //check if node is already linked
                foreach($node->field_related_projects_entity['und'] as $entlink):
                    if ($entlink['target_id'] == $obj['target_id']):
                        $alrady_linked = true;
                    endif;
                endforeach;
            endif;

            if (!$alrady_linked):
                array_push($node->field_related_projects_entity['und'], $obj);
            endif;
        // $node->field_related_projects_entity['und'][] = $obj; 
        // $node->field_related_projects_entity['und'][0]['target_id'] = $ref_id; 
        // $node->field_related_projects_entity['und'][0]['target_type'] = $ref_type; 
    endif;



    //SAVE NODE
    if($node = node_submit($node)) { 
        $node->changed = strtotime($last_modified->date); //set the modified date if desired
        node_save($node);
        //print_r($node);
        print "Fund Created\n";
        return $node->nid;
    }

    return false;
    
}

function createOrg($title, $ref_id=false, $ref_type='node'){
    print 'Relate Org: '.$title.'.to node id: '.$ref_id."\n";
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node')
      ->propertyCondition('type', 'organisation')
      ->propertyCondition('title', $title)
      ->propertyCondition('status', 1)
      ->range(0,1)
      ->execute();


      if (!empty($entities['node'])) {

        $node = node_load(array_shift(array_keys($entities['node'])));
        node_object_prepare($node);
        print 'ORG Already exists....'."\n";
        
        if ($node->nid):
            $obj = array();
            $obj['target_id'] = $ref_id;
            $obj['target_type'] = 'node';

            //print_r($obj);
             $alrady_linked = false;

            if ( !is_array($node->field_related_projects_entity['und']) ):
                $node->field_related_projects_entity['und'] = array();
            else:
                //check if node is already linked
                foreach($node->field_related_projects_entity['und'] as $entlink):
                    if ($entlink['target_id'] == $obj['target_id']):
                        $alrady_linked = true;
                    endif;
                endforeach;
            endif;

            if (!$alrady_linked):
                array_push($node->field_related_projects_entity['und'], $obj);
            endif;
            //$node->field_related_projects_entity['und'][] = $obj; 
            

            //print_r($node);


            node_save($node);
            
        else:
            return false;
        endif;
        return $node->nid;
      }

    print "Creating New Org\n";
    $node = new stdClass(); // x`Create a new node object
    $node->type = "organisation"; 
    node_object_prepare($node); // Set some default values
    $node->language = LANGUAGE_NONE;
    $node->status = 1; //<published>1</published> $node->field_joomla_status[$node->language][0]['value'] = 1;
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 1; //: 2 = comments open, 1 = comments closed, 0 = comments hidden
    $node->date = date("Y-m-d H:i:s");  //"2015-09-19 15:12:04"; 
    $node->uid = 31; //import user

    $node->title = $title; 

    //mark as donor
    $node->field_organisation_type[$node->language][]['tid'] = 154;


    //related content
    if ($ref_id):

        print 'setting related content: '.$ref_id;
         $obj = array();
        $obj['target_id'] = $ref_id;
        $obj['target_type'] = 'node';
        $alrady_linked = false;

            if ( !is_array($node->field_related_projects_entity['und']) ):
                $node->field_related_projects_entity['und'] = array();
            else:
                //check if node is already linked
                foreach($node->field_related_projects_entity['und'] as $entlink):
                    if ($entlink['target_id'] == $obj['target_id']):
                        $alrady_linked = true;
                    endif;
                endforeach;
            endif;

            if (!$alrady_linked):
                array_push($node->field_related_projects_entity['und'], $obj);
            endif;
        //$node->field_related_projects_entity['und'][] = $obj; 
        // $node->field_related_projects_entity['und'][0]['target_id'] = $ref_id; 
        // $node->field_related_projects_entity['und'][0]['target_type'] = $ref_type; 
    endif;

    //SAVE NODE
    if($node = node_submit($node)) { 
        $node->changed = strtotime($last_modified->date); //set the modified date if desired
        node_save($node);
        //print_r($node);
        print "Created Org\n";
        return $node->nid;
    }

    return false;
    
}


function createLink($title, $url, $ref_id=false, $ref_type='node'){
    print 'Create Link: '.$url."\n";
    $node = new stdClass(); // x`Create a new node object
    $node->type = "link"; 
    node_object_prepare($node); // Set some default values
    $node->language = LANGUAGE_NONE;
    $node->status = 1; //<published>1</published> $node->field_joomla_status[$node->language][0]['value'] = 1;
    $node->promote = 0; //(1 or 0): promoted to front page
    $node->comment = 1; //: 2 = comments open, 1 = comments closed, 0 = comments hidden
    $node->date = date("Y-m-d H:i:s");  //"2015-09-19 15:12:04"; 
    $node->uid = 31; //import user

    
    $node->title = $title; 

    //Description
    $node->field_description[$node->language][0]['value'] = $title; 
    //Url
    $node->field_url[$node->language][0]['value'] = $url; 

    //related content
    if ($ref_id):
        print 'setting related content: '.$ref_id;
        $obj = array();
        $obj['target_id'] = $ref_id;
        $obj['target_type'] = 'node';
        // array_push($node->field_related_content_entity['und'], $obj);
         $alrady_linked = false;

        if ( !is_array($node->field_related_content_entity['und']) ):
            $node->field_related_content_entity['und'] = array();
        else:
            //check if node is already linked
            foreach($node->field_related_content_entity['und'] as $entlink):
                if ($entlink['target_id'] == $obj['target_id']):
                    $alrady_linked = true;
                endif;
            endforeach;
        endif;

        if (!$alrady_linked):
            array_push($node->field_related_content_entity['und'], $obj);
        endif;
            
        // $node->field_related_content_entity['und'][] = $obj; 
        // $node->field_related_content_entity['und'][0]['target_id'] = $ref_id; 
        // $node->field_related_content_entity['und'][0]['target_type'] = $ref_type; 
    endif;

    //SAVE NODE
    if($node = node_submit($node)) { 
        $node->changed = strtotime($last_modified->date);          //set the modified date if desired
        node_save($node);
        //print_r($node);
        return $node->nid;
    }

    return false;
    
}

//createLink('test link','http://www.foo.com','3');
//die();

//Connect to the Mongo Database
$m = new MongoClient();
$db = $m->selectDB('pccp');
$collection = new MongoCollection($db, 'projects');

//Get all documents in mongo collection
$docs = $collection->find();

$count = 0;

$allthemes = array();

$limitImport = false;


foreach($docs as $doc):

    if ($doc['title'] == 'ACP-EU National Disaster Risk Reduction' || !$limitImport):
        print "************************************************************************\n";
        print 'Start Processing Project: '.$doc['title']."\n";
        print "************************************************************************\n";

        //check if exists
        $query = new EntityFieldQuery();
        $entities = $query->entityCondition('entity_type', 'node')
          ->propertyCondition('type', 'product')
          ->propertyCondition('title', $doc['title'])
          ->propertyCondition('status', 1)
          ->range(0,1)
          ->execute();

        if (!empty($entities['node'])):
            //project already exists
            
            $node = node_load(array_shift(array_keys($entities['node'])));
            node_object_prepare($node);
            print 'Project already exists -  ID : '.$node->nid.''."\n";
        else:
            //create new project
        
            $node = new stdClass(); // x`Create a new node object
            $node->type = "product"; 
            node_object_prepare($node); // Set some default values
            $node->language = LANGUAGE_NONE;
            $node->status = 1; 
            $node->promote = 0; //(1 or 0): promoted to front page
            $node->comment = 1; //: 2 = comments open, 1 = comments closed, 0 = comments hidden
            $node->date = "2015-09-19 15:12:04"; 
            $node->uid = 31; //import user

            $node->created = $doc['created'];
            $node->changed = $doc['changed'];

            
            $node->title = $doc['title']; 

            $node->field_old_drupal_id[$node->language][0]['value'] = $doc['nid']; 
            $node->field_verified_pccp[$node->language][0]['value'] = "0"; //default import value
            $node->field_imported_project[$node->language][0]['value'] = "1"; //default import value

        endif;

        //Description
        $node->field_the_description[$node->language][0]['format']  = 'full_html';  
        $node->field_the_description[$node->language][0]['value'] = trim(utf8_encode($doc['body']['und'][0]['safe_value']) ); 

        //text fields
        $node->field_objectives = $doc['field_project_objectives'];
        $node->field_outputs = $doc['field_project_outputs'];
        $node->field_short_title = $doc['field_project_short_name'];
        $node->field_actions = $doc['field_project_major_actions'];
        $node->field_activities = $doc['field_project_activities'];
        $node->field_amount_donor_currency = $doc['field_amount_donor_currency'];
        $node->field_project_total_funding = $doc['field_project_total_funding'];
        $node->field_duration = $doc['field_project_duration'];
        
        // Project Dates
        $dates = array();
        if (isset($doc['field_project_dates']['und'])):
            
            $terms_array = $doc['field_project_dates']['und'];
            foreach($terms_array as $k => $v):
                
                $start_year = date_parse_from_format("Y-m-d H:M:S", $v['value']);   
                $start = $start_year['year'];
                
                $end_year = date_parse_from_format("Y-m-d H:M:S", $v['value2']);    
                $end = $end_year['year'];



                if (isset($v['value'])):
                    print 'Start Date: '.$v['value']."\n";
                    $node->field_start_date[$node->language][0]['value'] = $v['value'];
                    $node->field_start_date[$node->language][0]['timezone'] = $v['timezone'];
                    $node->field_start_date[$node->language][0]['timezone_db'] = $v['timezone_db'];
                    $node->field_start_date[$node->language][0]['date_type'] = $v['date_type'];
                endif;
                if (isset($v['value2'])):
                    print 'End Date: '.$v['value2']."\n";
                    $node->field_end_date[$node->language][0]['value'] = $v['value2'];
                    $node->field_end_date[$node->language][0]['timezone'] = $v['timezone'];
                    $node->field_end_date[$node->language][0]['timezone_db'] = $v['timezone_db'];
                    $node->field_end_date[$node->language][0]['date_type'] = $v['date_type'];
                endif;

                // [value] => 2016-11-10T00:00:00
                // [timezone] => UTC
                // [timezone_db] => UTC
                // [date_type] => date
            endforeach;
        endif;


        //taxonomy fields

        // Project Type
        if (isset($doc['field_project_type']['und'])):
            $terms_array = $doc['field_project_type']['und'];
            foreach($terms_array as $k => $v):
                if (isset($v['tid_value'])):
                    $terms = taxonomy_get_term_by_name($v['tid_value'], 'project_type');
                    foreach ($terms as $term):
                        $node->field_project_type[$node->language][]['tid'] = $term->tid;
                    endforeach;
                endif;
            endforeach;
        endif;

         //status
        if (isset($doc['field_project_status']['und'])):
            $terms_array = $doc['field_project_status']['und'];
            foreach($terms_array as $k => $v):
                if (isset($v['tid_value'])):
                   $terms = taxonomy_get_term_by_name($v['tid_value'],'project_status');
                    foreach ($terms as $term):
                        $node->field_project_status[$node->language][]['tid'] = $term->tid;
                    endforeach;
                endif;
            endforeach;
        endif;

         //Project Scoee
        if (isset($doc['field_project_scope']['und'])):
            $terms_array = $doc['field_project_scope']['und'];
            foreach($terms_array as $k => $v):
                if (isset($v['tid_value'])):
                    $terms = taxonomy_get_term_by_name($v['tid_value'],'project_scope');
                    foreach ($terms as $term):
                        $node->field_project_scope[$node->language][]['tid'] = $term->tid;
                    endforeach;
                endif;
            endforeach;
        endif;


         //PIFACC Themes
        if (isset($doc['field_project_pifacc']['und'])):
            $terms_array = $doc['field_project_pifacc']['und'];
            foreach($terms_array as $k => $v):
                if (isset($v['tid_value'])):
                    if ($v['tid_value'] != 'Regional' && $v['tid_value'] != 'National'):
                        $terms = taxonomy_get_term_by_name($v['tid_value'],'pifacc27_95901');
                        foreach ($terms as $term):
                            $node->field_pifacc_themes[$node->language][]['tid'] = $term->tid;
                        endforeach;
                    endif;
                endif;
            endforeach;
        endif;



        // //Topics
        // if (isset($doc['themes']) && count($doc['themes'])):
        //  $l0_topics_array = array();
        //  $l1_topics_array = array();
        //  foreach ($doc['themes'] as $scope):
        //      //debug($scope);
        //      $sector_url_prefix = 'http://projects.pacificclimatechange.net/climate-change-projects/sectors/';
        //      if (!empty($scope['theme']['href']) ):
        //          $theme = $scope['theme']['href'];
        //          $theme = str_replace($sector_url_prefix, '', $theme);
                    
        //          debug($theme);
        //          $topics_l0 = $this->mapLevel0Topic( $theme );
        //          $topics_l1 = $this->mapLevel1Topic( $theme );

        //          foreach($topics_l0 as $l0_topic){
        //              $l0_topics_array[$l0_topic] = $l0_topic;
        //              //$solr_doc->addField('vocab_topics_level0', $l0_topic);
        //          }
                    
        //          foreach($topics_l1 as $l1_topic){
        //              $l1_topics_array[$l1_topic] = $l1_topic;
        //              //$solr_doc->addField('vocab_topics_level1', $l1_topic);
        //          }
        //      endif;

                
        //      if (!empty($scope['theme']['text']) ):
        //          //$solr_doc->addField('project_theme_ss', $scope['theme']['text']);
        //          //$solr_doc->addField('topics_ss', $scope['theme']['text']);
        //          $topics_l0 = $this->mapLevel0Topic( $scope['theme']['text'] );
        //          $topics_l1 = $this->mapLevel1Topic( $scope['theme']['text'] );

        //          foreach($topics_l0 as $l0_topic){
        //              $solr_doc->addField('vocab_topics_level0', $l0_topic);
        //          }
                    
        //          foreach($topics_l1 as $l1_topic){
        //              $solr_doc->addField('vocab_topics_level1', $l1_topic);
        //          }
        //      endif;
                
        //  endforeach;


        //  debug($l0_topics_array);
        //  debug($l1_topics_array);


        //  foreach($l0_topics_array as $l0_topic){
        //      $solr_doc->addField('vocab_topics_level0', $l0_topic);
        //  }

            
        //  foreach($l1_topics_array as $l1_topic){
        //      //$l1_topics_array[$l1_topic] = $l1_topic;
        //      $solr_doc->addField('vocab_topics_level1', $l1_topic);
        //  }
        // endif;



        // SAVE NODE
        if($node = node_submit($node)) { 
            $node->changed = strtotime($last_modified->date);          //set the modified date if desired
            node_save($node);
        }

        //store the new node id in mongo
        $doc['new_pccp_node_id'] = $node->nid;
        $collection->update(array('nid' => $doc['nid']),$doc);




        //entity links
        //Project Contacts
        if (isset($doc['field_project_contact']['und'])):
            $terms_array = $doc['field_project_contact']['und'];
            foreach($terms_array as $k => $v):
                if (isset($v['tid_value'])):
                    
                    $contact_node_id = createContact($v['tid_value'], $node->nid);

                    if ($contact_node_id):
                        //create a reference to the link entity
                        $obj = array();
                        $obj['target_id'] = $contact_node_id;
                        $obj['target_type'] = 'node';
                        // array_push($node->field_related_links_entity['und'], $obj);
                        $node->field_project_contact[$node->language][] = $obj;
                        // $node->field_related_links_entity[$node->language][0][]['target_id'] = $contact_node_id; 
                        // $node->field_related_links_entity[$node->language][0][]['target_type'] = 'node'; 
                    endif;

                endif;
                
                
            endforeach;
        endif;

        //Implementing agency
        if (isset($doc['field_project_implement_agency']['und'])):
            $terms_array = $doc['field_project_implement_agency']['und'];
            foreach($terms_array as $k => $v):
                //$doc['field_project_donors']['und'][$k] = $terms[$v['tid']];
                if ( isset($v['tid_value']) ):
                    //create org
                    $org_node_id = createOrg($v['tid_value'], $node->nid);
                    if ($org_node_id):
                        $obj = array();
                        $obj['target_id'] = $org_node_id;
                        $obj['target_type'] = 'node';

                        if ( !is_array($node->field_implementing_agency['und']) ):
                            $node->field_implementing_agency['und'] = array();
                        endif;
                        array_push($node->field_implementing_agency['und'], $obj);
                        // $node->field_implementing_agency[$node->language][] = $obj;
                        // $node->field_implementing_agency[$node->language][]['target_id'] = $ent->nid;
                        // $node->field_implementing_agency[$node->language][]['target_type'] = 'node';
                    endif;
                endif;
                
                //print $terms[$v['tid']];
            endforeach;
        endif;
        


         //Donors
        if (isset($doc['field_project_donors']['und'])):
            $terms_array = $doc['field_project_donors']['und'];
            foreach($terms_array as $k => $v):
                if ( isset($v['tid_value']) ):
                    // if ( strpos( strtolower( $v['tid_value'] ), 'fund') !== false ):
                    //     //create fund
                    //     $fund_id = createFund($v['tid_value'], $node->nid);
                    //     if ($fund_id):
                    //         $obj = array();
                    //         $obj['target_id'] = $fund_id;
                    //         $obj['target_type'] = 'node';
                    //         $node->field_project_donor[$node->language][] = $obj;
                    //     endif;
                    // else:
                        //create org
                        $org_id = createOrg($v['tid_value'], $node->nid);
                        if ($org_id):
                            $obj = array();
                            $obj['target_id'] = $org_id;
                            $obj['target_type'] = 'node';
                            if ( !is_array($node->field_donor_enity['und']) ):
                                $node->field_donor_enity['und'] = array();
                            endif;
                            array_push($node->field_donor_enity['und'], $obj);
                            //$node->field_project_donor[$node->language][] = $obj;
                        endif;
                    // endif;
                endif;
                
                //print $terms[$v['tid']];
            endforeach;
        endif;



        //Countries
        if (isset($doc['field_project_country']['und'])):
            $terms_array = $doc['field_project_country']['und'];
            foreach($terms_array as $k => $v):
                if (isset($v['tid_value'])):
                    // find entities
                    $countryid = createCountry($v['tid_value'], $node->nid);
                    if ( isset($countryid) ):
                                $obj = array();
                                $obj['target_id'] = $countryid;
                                $obj['target_type'] = 'node';
                                //print_r($obj);
                                //array_push($node->field_implementing_countries['und'], $obj);
                                if (!is_array($node->field_implementing_countries[$node->language])):
                                    $node->field_implementing_countries[$node->language] = array();
                                endif;
                                array_push($node->field_implementing_countries[$node->language],$obj);
                                //$node->field_implementing_countries[$node->language][] = $obj;
                    endif;
                    // $query = new EntityFieldQuery();
                    // $entities = $query->entityCondition('entity_type', 'node')
                    //   ->propertyCondition('type', 'country')
                    //   ->propertyCondition('title', $v['tid_value'])
                    //   ->propertyCondition('status', 1)
                    //   ->range(0,1)
                    //   ->execute();


                    //   if (!empty($entities['node'])) {
                    //     //$node = node_load(array_shift(array_keys($entities['node'])));
                    //     foreach ($entities['node'] as $ent):
                    //         //print_r($ent->nid);
                    //         if ( isset($ent->nid) ):
                    //             $obj = array();
                    //             $obj['target_id'] = $ent->nid;
                    //             $obj['target_type'] = 'node';
                    //             //print 'FOUND COUNTRY:: '.$ent->nid."\n";
                    //             //print_r($obj);
                    //             //array_push($node->field_implementing_countries['und'], $obj);
                    //             $node->field_implementing_countries[$node->language][] = $obj;
                    //             //print 'COUNTRY nodes: '."\n";
                    //             //print_r($node->field_implementing_countries['und']);
                                
                                

                    //             //create reverse links
                    //             createReverseLink( $ent->nid, $node->nid,'field_related_projects_entity');
                    //         endif;
                    //     endforeach;
                    //   }
                endif;
            endforeach;
        endif;
        

        //Project Links
        if (isset($doc['field_poject_result_links']['und'])):
            $terms_array = $doc['field_poject_result_links']['und'];
            foreach($terms_array as $k => $v):
                if (isset($v['url'])):
                    $url = $v['url'];
                    $title = $v['url'];
                    if (isset($v['title'])):
                        $title = $v['title'];
                    endif;
                    $link_node_id = createLink($title, $url, $node->nid);

                    if ($link_node_id):
                        //create a reference to the link entity
                        $obj = array();
                        $obj['target_id'] = $link_node_id;
                        $obj['target_type'] = 'node';
                        
                        //print_r($obj);
                        //array_push($node->field_related_links_entity['und'], $obj);
                        $node->field_related_links_entity[$node->language][] = $obj;
                        // $node->field_related_links_entity[$node->language][0][]['target_id'] = $link_node_id; 
                        // $node->field_related_links_entity[$node->language][0][]['target_type'] = 'link'; 
                    endif;
                endif;
                

                
            endforeach;
        endif;

        


        //SAVE NODE AGAIN
        // print 'BEFORE 2nd SAVE';
        // print_r($node);
        node_save($node);
        
        print "************************************************************************\n";
        print 'END Processing Project: '.$doc['title']."\n";
        print "************************************************************************\n";

        $count++;
    

    endif;
    

endforeach;


print 'Done';