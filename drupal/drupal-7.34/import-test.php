<?php
// Tutorial
// https://anders.unix.se/2011/04/13/guide-to-programmatic-node-creation-in-drupal-7/
// http://oit.franklin.uga.edu/content/programmatically-importing-drupal-nodes

//id=0,bookid=1,isbn=2,title=3,authors=4,
//manufacturer=5,release_Date=6,language=7,hits=8,rating=9,

//price,priceunit,numberOfPages=12,URL=13,imageURL=14
//,edition=15,ebookURL=16,informationFrom,date=18,comment=19,published=20,owneremail=21,catid=22,cat_title=23

define('DRUPAL_ROOT', getcwd());
$_SERVER['REMOTE_ADDR'] = "localhost"; // Necessary if running from command line
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$row = 1;
if (($handle = fopen("booklibrary-for-drupal-export-updated.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        $num = count($data);
        //echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        //for ($c=0; $c < $num; $c++) {
            //echo $data[$c] . "<br />\n";
           
            //echo $data[16];
            $node = new stdClass(); // Create a new node object
            $node->type = "document"; 
            node_object_prepare($node); // Set some default values
            $node->language = LANGUAGE_NONE;

            //$wrapper = entity_metadata_wrapper('node', $node);
            //$e->uid = $user->uid;
            $node->title = $data[3]; //<title><![CDATA[Rapid Assessment Report – Tuvalu {November 2012)]]></title>

            $node->field_joomla_id[$node->language][0]['value'] = $data[1];//"USP26"; // <bookid></bookid>
            $node->field_joomla_hits[$node->language][0]['value'] = $data[8];//10; //<hits>14</hits>
            $node->status = $data[20]; //<published>1</published> $node->field_joomla_status[$node->language][0]['value'] = 1;
            $node->promote = 0; //(1 or 0): promoted to front page
            //$node->created = $data[18];
            //$node->sticky (1 or 0): sticky at top of lists
            //$node->comment: 2 = comments open, 1 = comments closed, 0 = comments hidden

            $node->field_publisher[$node->language][0]['value'] = $data[5]; //"UNDP"; //<manufacturer><![CDATA[USP]]></manufacturer>
            $node->field_physical_description[$node->language][0]['format']  = 'full_html';  //set the format for the body i.e. full_html; the default is the same as the drupal default
            $node->field_physical_description[$node->language][0]['value'] = trim(utf8_encode($data[12]) ); //<numberOfPages>4</numberOfPages>

            //$date = date_create($data[18]);
            //echo date_format($date, 'Y-m-d H:i:s');
            //echo $data[18];
            $node->date = $data[18];//2015-04-27 17:18:30";//strval($data[18]);//date_format($data[18],'Y-m-d H:i:s'); //<date>2015-04-27 17:18:30</date>
            
            $node->field_the_description[$node->language][0]['format']  = 'full_html';  //set the format for the body i.e. full_html; the default is the same as the drupal default
            $node->field_the_description[$node->language][0]['value'] = trim(utf8_encode($data[19]) ); // <comment> tag

            if(intval($data[6])>1800){
                $node->field_publication_year[$node->language][0]['value'] = intval($data[6]); //2000; //<releaseDate></releaseDate>
            }
            $node->field_edition[$node->language][0]['value'] = $data[15]; //'First Edition'; //<edition><![CDATA[]]></edition>
            $node->field_isbn[$node->language][0]['value'] = $data[2]; //'ISBN-1000000002222'; //<isbn></isbn>
            //$node->field_issn[$node->language][0]['value'] = $data[8]; //'ISSN-2000000001111';

            $node->field_author[$node->language][0]['format']  = 'full_html';  //set the format for the body i.e. full_html; the default is the same as the drupal default
            $node->field_author[$node->language][0]['value'] = trim(utf8_encode($data[4]) ); //<authors><![CDATA[]]></authors>

            // Map old category to new TOPICS catid=22, cat_title=23
            $foo = taxonomy_get_term_by_name($data[23]);
            //$foo = asort($foo)
            //if(count($foo)>1) { echo $data[23] . ">1 ";}
            $topicSet = false;
            if(empty($foo)){ // if TOPIC does not exist - add as a TAG
                $term = new stdClass();
                $term->name = $data[23];
                $term->vid = 1; // 1 = vid for TAGS
                taxonomy_term_save($term); // add new TAG term
                $node->field_tags[$node->language][]['tid'] = $term->tid; // set TAG // possibly null or blank. need data check on drupal
            }else{
                foreach ($foo as $term) { //echo $term->tid . ":" . $term->name . " - ";
                    if ($term->vid == 2) { // 2 = vid for topics
                        $node->field_topics[$node->language][]['tid'] = $term->tid; // set TOPIC
                        $topicSet = true;
                    }elseif ($term->vid ==1 && $topicSet==false) {
                        $node->field_tags[$node->language][]['tid'] = $term->tid; // set TAG
                    }
                } 
            }
            // Add Language - field from joomla document library //$node->field_language[$node->language][0]['value'] = 1; //<language>English</language>
            /*$foo = taxonomy_get_term_by_name('Samoan');
            foreach ($foo as $term) {
                if ($term->vid == 18) { // 18 = vid for language vocabulary
                    $node->field_language[$node->language][]['tid'] = $term->tid;
                }
            } */
            //$node->field_language[$node->language][0]['tid'] = 203;

            //echo basename($data[16]) . " - " . substr($data[16], 0, 4) . " : ";
            // Attach pdf/docs to the node
            //<ebookURL><![CDATA[/components/com_booklibrary/ebooks/RAPID ASSESSMENT REPORT tuvalu.pdf]]></ebookURL>
            if(substr($data[16], 0, 4) == 'http'){ // if its a web link - add to [Document Link] field
                $node->field_document_link[$node->language][0]['value'] = $data[16];
            } else {

                $file_path = drupal_realpath('sites/default/files/ebooks/' . basename($data[16])); // Create a File object - replace with local files
                $file = (object) array(
                          'uid' => 1,
                          'uri' => $file_path,
                          'filemime' => file_get_mimetype($filepath),
                          'status' => 1,
                          'display' => 1,
                          'description' => basename($data[16])
                ); 
                
                $file = file_copy($file, 'public://documents'); // Save the file to the root of the files directory. You can specify a subdirectory, for example, 'public://images' 
                $node->field_document_files[$node->language][0] = (array) $file; 
                $node->field_document_link[$node->language][0]['value'] = $file->uri; //add to [Document Link] field
            }
            //echo file_exists($file->uri);
            //echo $file->uri . " " . $file->description;
            //node_save($node);
            //associate the file object with the image field:
            //$wrapper->field_cover_image = (array)$file;

            // Adding Book Cover 
            $file_path = drupal_realpath('sites/default/files/cover/'. basename($data[14])); //<imageURL><![CDATA[/components/com_booklibrary/covers/-_395a531b.jpg]]></imageURL>
            $file = (object) array(
                'uid' => 1,
                'uri' => $file_path,
                'filemime' => file_get_mimetype($file_path),
                'status' => 1,
                'display' => 1,
                //'description' => "some pic here"
            );
            // You can specify a subdirectory, e.g. public://foo/
            $file = file_copy($file, 'public://covers');
            $node->field_cover_image[$node->language][0] = (array) $file;

            if($node = node_submit($node)) { 
                $node->changed = strtotime($last_modified->date);          //set the modified date if desired
                node_save($node);
            }

            //print_r(node_load(2));
            //<url><![CDATA[]]></url>
            //<rating>0</rating>
            //<price></price>
            //<priceunit></priceunit>
            //<informationFrom>0</informationFrom>
            //<reviews></reviews>
            //<owneremail><![CDATA[priscillao@sprep.org]]></owneremail>
            //<categs><categ>290</categ></categs>
        //}
    }
    fclose($handle);
}
        //file_create_url('public://php_includes/functions.php');
//    }//end while $data = fgetcsv
// fclose($handle);
// }//end if
// Document URL
            //<ebookURL><![CDATA[/components/com_booklibrary/ebooks/RAPID ASSESSMENT REPORT tuvalu.pdf]]></ebookURL>
            /*$filepath =  $_SERVER['DOCUMENT_ROOT'].base_path() . 'sites/default/files/documents/SPREP AGM BOOKING FORM.pdf';//
            //echo file_exists($filepath);// .drupal_get_path('module','payment_details').'/MDSO_20.jpg';
            //$file_path = 'sites/default/files/documents/SPREP AGM BOOKING FORM.pdf';
            echo $filepath . file_exists($file);
            $file = (object) array(
                'uid' => 1,
                'uri' => $file_path,
                'filemime' => file_get_mimetype($file_path),
                'status' => 1,
            );
            //echo file_exists($file);
            // You can specify a subdirectory, e.g. public://foo/
            //$file = file_copy($file, 'public://');
            //$node->field_image[LANGUAGE_NONE][0] = (array)$file;
            //node_save($node);
            $node->field_download_url[$node->language][0] = (array)$file;//"sites/default/files/documents/SPREP AGM BOOKING FORM.pdf";//(array) $file;
            */
