<?php
// import news articles
//title=0,introtext=1,state=2,created=3,hits=4,ordering=5

define('DRUPAL_ROOT', getcwd());
$_SERVER['REMOTE_ADDR'] = "localhost"; // Necessary if running from command line
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

$row = 1;
if (($handle = fopen("pccp_latest_news_export.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        $num = count($data);
        //echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
   
            $node = new stdClass(); // Create a new node object
            $node->type = "news"; 
            node_object_prepare($node); // Set some default values
            $node->language = LANGUAGE_NONE;

        
            $node->title = $data[0];

            $node->field_text[$node->language][0]['format']  = 'full_html';
            $node->field_text[$node->language][0]['value'] = $data[1];
            
            $node->status = $data[2]; 
            $node->promote = 0;
            $node->date = $data[3];	
	
        	if(intval($data[4]>0)){
                    $node->field_joomla_hits[$node->language][0]['value'] = intval($data[4]);
        	}
            print_r($node);
            if($node = node_submit($node)) { 
                $node->changed = strtotime($last_modified->date);          //set the modified date if desired
                node_save($node);
            }
    }
    fclose($handle);
}

