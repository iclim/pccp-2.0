<?php

$config = array(
    'endpoint' => array(
        'localhost' => array(
            'host' => '127.0.0.1',
            'port' => 8080,
            'path' => '/solr/',
            'core' => 'collection1',
        )
    )
);
