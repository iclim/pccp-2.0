function showAll(facetname){
	
	
    $('ul.'+facetname+'-facet li').each(
      function(){
          $(this).removeClass('hidden');
      }
    );

}

jQuery(document).ready(function($) {
	

	$('#fed-search').typeahead({
	  minLength: 3,
	  highlight: true,

	},
	{
	  name: 'solr-suggests',
	  source: function (query, process) {
	        //return $.get('http://'+document.domain+':8090/solr/suggest', { q: query }, function (data) {
	        terms = [];
	        map = {};
	        var solrResult = [];
	        $.ajax({
			  'url': 'http://203.101.226.109:8080/solr/suggest/',
			  'data': {'wt':'json', 'q':query},
			  'success': function(data) { 
			  		solrResult = data.suggest.default[query].suggestions; 
			  		$.each(solrResult, function (i, term) {
			  			term.value = term.term;
			  			console.log(term.term);
				        map[term.term] = term.term;
				        terms.push(term);
				    });

				    //console.log(terms);
				 
				    process(terms);
			  },
			  'dataType': 'jsonp',
			  'jsonp': 'json.wrf'
			});	

			//console.log(solrResult);

	    }
	});

	var appendQuery = function(query){
		window.location = '?' + query;
	}

	$('.change-filter').change(function(){
		appendQuery($(this).data('query'));
	});



    



});