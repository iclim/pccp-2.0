<?php

/**
 * @file
 * template.php
 */

if (!function_exists('pccpbootstrap_preprocess_page')) {
  function pccpbootstrap_preprocess_page(&$variables, $hook) {  // Add theme suggestion for all content types
  	
  	// if this is a panel page, add template suggestions
  	  if($panel_page = page_manager_get_current_page()) {
  	    // add a generic suggestion for all panel pages
  	    $variables['theme_hook_suggestions'][] = 'page__panel';

  	    // add the panel page machine name to the template suggestions
  	    $variables['theme_hook_suggestions'][] = 'page__' . $panel_page['name'];

  	    //add a body class for good measure
  	    $body_classes[] = 'page-panel';
  	  }

      if (isset($variables['node'])) {
          if ($variables['node']->type != '') {
              $variables['theme_hook_suggestions'][] = 'page__node__' . $variables['node']->type;
          }
      }
  }
}


if (!function_exists('get_breadcrumb')) {
  function get_breadcrumb() {
  	$breadcrumb = array();
      $breadcrumb[] = l(t('Home'), '<front>');
      $is_tax = false;	

    if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    	$is_tax = true;

      $breadcrumb[] = l(t('Taxonomy'), '#');

      $tid = arg(2);

      if ($term = taxonomy_term_load($tid)) {
        $uri = entity_uri('taxonomy_term', $term);
        $breadcrumb[] = l($term->name, $uri['path'], $uri['options']);
      }
    }

    // resume normal operation
    if (!empty($breadcrumb)) {
    // uncomment the next line to enable current page in the breadcrumb trail
    	if (!$is_tax):
  	    $title = drupal_get_title();
  	    if (!empty($title)) {
  	      $breadcrumb[] = $title;
  	      unset($title);
  	    }
      endif;

      return '<div class="breadcrumb">'. implode(' &gt; ', $breadcrumb) . '</div>';
    }
  }
}