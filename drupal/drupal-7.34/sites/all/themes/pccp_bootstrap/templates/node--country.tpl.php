
<!-- <pre> -->
<?php 


//print_r($node); 
//print drupal_get_path_alias('node/46');
//Prepare Document
 // We hide the comments and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);

hide($content['field_topics']);
hide($content['field_focus_area']);
hide($content['field_related_links_entity']);
hide($content['field_related_projects_entity']);
hide($content['field_related_documents_entity']);
hide($content['field_related_news']);
hide($content['field_related_links_entity']);
hide($content['field_related_events']);


hide($content['field_verified_pccp']);
hide($content['field_related_corporations']);
hide($content['field_related_name_s_']);
hide($content['field_document_files']);


hide($content['field_iso_code']);




//var_dump($content['field_project_status'])


?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php if ($title_prefix || $title_suffix || $display_submitted || !$page): ?>
  <header>
    <?php print render($title_prefix); ?>
    
    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a>
       <?php if (($tags = render($content['field_flag_image'])) ): ?>  
            <?php print render($content['field_flag_image']); ?>
        <?php endif; ?> 
      </h2>

    <?php endif; ?>
    <?php print render($title_suffix); ?>

    
  </header>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>


    <div class="container country-header">
        <div class="col-md-4">
            <?php if (($tags = render($content['field_flag_image'])) ): ?>  
                <span class="flag-image">
                    <?php print render($content['field_flag_image']); ?>
                </span>
            <?php endif; ?> 
        </div>
        
        <div class="col-md-8">
            <?php if (($tags = render($content['field_map_image'])) ): ?>  
                <span class="map-image">
                    <?php print render($content['field_map_image']); ?>
                </span>
            <?php endif; ?> 
        </div>

        <div class="pull-right">
            <?php echo print_pdf_insert_link(); ?>
        </div>
        
    </div>

      <div class="container">


        

       

       

      </div>


    <div class="container">

      <!-- Nav tabs -->
      <ul class="nav nav-pills nav-stacked col-md-3" role="tablist">
        <li role="presentation active" class="active"><a href="#overview"     aria-controls="overview"      role="tab" data-toggle="tab">Overview</a></li>
        <li role="presentation"><a href="#priorities"  aria-controls="priorities"   role="tab" data-toggle="tab">National Priorities</a></li>
        <li role="presentation"><a href="#governance" aria-controls="adaption"  role="tab" data-toggle="tab">Governance</a></li>
        <li role="presentation"><a href="#policies" aria-controls="policies"  role="tab" data-toggle="tab">Key National Policies, Plans and Actions</a></li>
        <li role="presentation"><a href="#adaption" aria-controls="adaption"  role="tab" data-toggle="tab">Adaptation</a></li>
        <li role="presentation"><a href="#partnerships" aria-controls="partnerships"  role="tab" data-toggle="tab">Partnerships</a></li>
        <li role="presentation"><a href="#climateinfo" aria-controls="climateinfo"  role="tab" data-toggle="tab">Climate Information</a></li>
        <li role="presentation"><a href="#education" aria-controls="education"  role="tab" data-toggle="tab">Education</a></li>
        <li role="presentation"><a href="#mitigation" aria-controls="mitigation"  role="tab" data-toggle="tab">Mitigation</a></li>
        <li role="presentation"><a href="#ongoing" aria-controls="ongoing"  role="tab" data-toggle="tab">Ongoing Challenges and Gaps</a></li>
        <li role="presentation"><a href="#focalpoints" aria-controls="focalpoints"  role="tab" data-toggle="tab">Focal points</a></li>
        <li role="presentation"><a href="#references" aria-controls="references"  role="tab" data-toggle="tab">References</a></li>
        <?php if (($tags = render($content['field_related_projects_entity'])) ): ?>
            <?php
            if ( isset($content['field_related_projects_entity']['#items']) ):
                $related = $content['field_related_projects_entity']['#items'];
                $counter = 0;
                if (is_array($related)):
                    foreach ($related as $entity) {
                        $counter++;
                    }
                endif;
            endif;
            ?>
            <li role="presentation"><a href="#related_projects" aria-controls="related_projects"  role="tab" data-toggle="tab">Related Projects (<?php print $counter;?>)</a></li>
        <?php endif; ?>
        <?php if (($tags = render($content['field_related_documents_entity'])) ): ?>
            <?php
            if ( isset($content['field_related_documents_entity']['#items']) ):
                $related = $content['field_related_documents_entity']['#items'];
                $counter = 0;
                if (is_array($related)):
                    foreach ($related as $entity) {
                        $counter++;
                    }
                endif;
            endif;
            ?>
            <li role="presentation"><a href="#related_docs" aria-controls="related_docs"  role="tab" data-toggle="tab">Related Documents (<?php print $counter;?>)</a></li>
        <?php endif; ?>
        <?php if (($tags = render($content['field_related_links_entity'])) ): ?>
            <?php
            if ( isset($content['field_related_links_entity']['#items']) ):
                $related = $content['field_related_links_entity']['#items'];
                $counter = 0;
                if (is_array($related)):
                    foreach ($related as $entity) {
                        $counter++;
                    }
                endif;
            endif;
            ?>
            <li role="presentation"><a href="#related_links" aria-controls="related_links"  role="tab" data-toggle="tab">Related Links (<?php print $counter;?>)</a></li>
        <?php endif; ?>

        <?php if (($tags = render($content['field_related_news'])) ): ?>
             <?php
            if ( isset($content['field_related_news']['#items']) ):
                $related = $content['field_related_news']['#items'];
                $counter = 0;
                if (is_array($related)):
                    foreach ($related as $entity) {
                        $counter++;
                    }
                endif;
            endif;
            ?>
            <li role="presentation"><a href="#related_news" aria-controls="related_news"  role="tab" data-toggle="tab">Related News (<?php print $counter;?>)</a></li>
        <?php endif; ?>

      </ul>

      <!-- Tab panes -->
      <div class="tab-content col-md-6">
        <div role="tabpanel" class="tab-pane active" id="overview"><?php print render($content['field_country_overview']); ?></div>
        <div role="tabpanel" class="tab-pane" id="priorities"><?php print render($content['field_country_national_prioritie']); ?></div>
        <div role="tabpanel" class="tab-pane" id="governance"><?php print render($content['field_country_governance']); ?></div>
        <div role="tabpanel" class="tab-pane" id="policies"><?php print render($content['field_country_key_national_poli']); ?></div>
        <div role="tabpanel" class="tab-pane" id="adaption"><?php print render($content['field_country_adaptation']); ?></div>
        <div role="tabpanel" class="tab-pane" id="partnerships"><?php print render($content['field_country_partnerships']); ?></div>
        <div role="tabpanel" class="tab-pane" id="climateinfo"><?php print render($content['field_country_climate_informatio']); ?></div>
        <div role="tabpanel" class="tab-pane" id="education"><?php print render($content['field_country_education']); ?></div>
        <div role="tabpanel" class="tab-pane" id="mitigation"><?php print render($content['field_country_mitigation']); ?></div>
        <div role="tabpanel" class="tab-pane" id="ongoing"><?php print render($content['field_country_ongoing_challenges']); ?></div>
        <div role="tabpanel" class="tab-pane" id="focalpoints"><?php print render($content['field_country_focal_points']); ?></div>
        <div role="tabpanel" class="tab-pane" id="references"><?php print render($content['field_country_references']); ?></div>
        <div role="tabpanel" class="tab-pane" id="related_projects">
        <?php 
                    if ( isset($content['field_related_projects_entity']['#items']) ):
                        $related = $content['field_related_projects_entity']['#items'];
                        if (is_array($related)):
                                foreach ($related as $entity) {
                                    //var_dump($entity);
                                    $title = $entity['entity']->title;
                                    $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                    print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                                }
                            endif;
                        endif;
                    ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="related_docs">
         <?php 
                    if ( isset($content['field_related_documents_entity']['#items']) ):
                        $related = $content['field_related_documents_entity']['#items'];
                        if (is_array($related)):
                                foreach ($related as $entity) {
                                    //var_dump($entity);
                                    $title = $entity['entity']->title;
                                    $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                    print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                                }
                            endif;
                        endif;
                    ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="related_links">
         <?php 
                    if ( isset($content['field_related_links_entity']['#items']) ):
                        $related = $content['field_related_links_entity']['#items'];
                        if (is_array($related)):
                                foreach ($related as $entity) {
                                    //var_dump($entity);
                                    $title = $entity['entity']->title;
                                    $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                    print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                                }
                            endif;
                        endif;
                    ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="related_news">
         <?php 
                    if ( isset($content['field_related_news']['#items']) ):
                        $related = $content['field_related_news']['#items'];
                        if (is_array($related)):
                                foreach ($related as $entity) {
                                    //var_dump($entity);
                                    $title = $entity['entity']->title;
                                    $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                    print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                                }
                            endif;
                        endif;
                    ?>
        </div>
      </div>

    


    <div class="col-md-3">
        <!-- Dashboard -->

        <?php if (($tags = render($content['field_related_documents_entity'])) ): ?>
            
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Documents</h3>
                    </div>
                    <div class="panel-body">
                        <div class="field-items">
                        <?php 
                        // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                        // print render($field) 
                        if ( isset($content['field_related_documents_entity']['#items']) ):
                        $related = $content['field_related_documents_entity']['#items'];
                        if (is_array($related)):
                                $counter = 0;
                                foreach ($related as $entity) {
                                    $counter++;
                                    if ($counter <= 3):
                                        //var_dump($entity);
                                        $title = $entity['entity']->title;
                                        $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                        print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                                    endif;

                                }
                            endif;
                        endif;
                        ?>
                        </div>
                    </div>
                </div>
            
        <?php endif; ?> 

        
        <?php if (($tags = render($content['field_related_projects_entity'])) ): ?>
            
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Projects</h3>
                    </div>
                    <div class="panel-body">
                        <div class="field-items">
                        <?php 
                        // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                        // print render($field) 
                        if ( isset($content['field_related_projects_entity']['#items']) ):
                        $related = $content['field_related_projects_entity']['#items'];
                        if (is_array($related)):
                                $counter = 0;
                                foreach ($related as $entity) {
                                    $counter++;
                                    if ($counter <= 3):
                                        //var_dump($entity);
                                        $title = $entity['entity']->title;
                                        $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                        print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                                    endif;

                                }
                            endif;
                        endif;
                        ?>
                        </div>
                    </div>
                </div>
            
        <?php endif; ?> 

        <?php if (($tags = render($content['field_related_links_entity'])) ): ?>
            
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Links</h3>
                    </div>
                    <div class="panel-body">
                        <div class="field-items">
                        <?php 
                        // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                        // print render($field) 
                        if ( isset($content['field_related_links_entity']['#items']) ):
                        $related = $content['field_related_links_entity']['#items'];
                        if (is_array($related)):
                                $counter = 0;
                                foreach ($related as $entity) {
                                    $counter++;
                                    if ($counter <= 3):
                                        //var_dump($entity);
                                        $title = $entity['entity']->title;
                                        $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                        print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                                    endif;

                                }
                            endif;
                        endif;
                        ?>
                        </div>
                    </div>
                </div>
            
        <?php endif; ?> 

         <?php if (($tags = render($content['field_related_news'])) ): ?>
            
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent News</h3>
                    </div>
                    <div class="panel-body">
                        <div class="field-items">
                        <?php 
                        // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                        // print render($field) 
                        if ( isset($content['field_related_news']['#items']) ):
                        $related = $content['field_related_news']['#items'];
                        if (is_array($related)):
                                $counter = 0;
                                foreach ($related as $entity) {
                                    $counter++;
                                    if ($counter <= 3):
                                        //var_dump($entity);
                                        $title = $entity['entity']->title;
                                        $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                        print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                                    endif;

                                }
                            endif;
                        endif;
                        ?>
                        </div>
                    </div>
                </div>
            
        <?php endif; ?> 

        <?php if (($tags = render($content['field_related_events'])) ): ?>
            
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Events</h3>
                    </div>
                    <div class="panel-body">
                        <div class="field-items">
                        <?php 
                        // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                        // print render($field) 
                        if ( isset($content['field_related_events']['#items']) ):
                        $related = $content['field_related_events']['#items'];
                        if (is_array($related)):
                                $counter = 0;
                                foreach ($related as $entity) {
                                    $counter++;
                                    if ($counter <= 3):
                                        //var_dump($entity);
                                        $title = $entity['entity']->title;
                                        $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                        print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                                    endif;

                                }
                            endif;
                        endif;
                        ?>
                        </div>
                    </div>
                </div>
            
        <?php endif; ?> 
    </div>

    </div>

    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_tags']);
      print render($content);
    ?>
  </div>
    
    <?php if (($tags = render($content['field_tags'])) || ($links = render($content['links']))): ?>
    <footer>
    <?php if ($display_submitted): ?>
      <div class="submitted">
        <?php print $user_picture; ?>
        <span class="glyphicon glyphicon-calendar"></span> <?php print $submitted; ?>
      </div>
    <?php endif; ?>
    <?php print render($content['field_tags']); ?>
    <?php print render($content['links']); ?>
    </footer>
    <?php endif; ?> 

  <?php print render($content['comments']); ?>

</article>