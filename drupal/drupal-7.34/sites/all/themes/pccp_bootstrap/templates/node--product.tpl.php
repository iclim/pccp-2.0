
<!-- <pre> -->
<?php 


//print_r($node); 
//print drupal_get_path_alias('node/46');
//Prepare Document
 // We hide the comments and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);

hide($content['field_topics']);
hide($content['field_focus_area']);
hide($content['field_related_documents_entity']);
hide($content['field_related_projects_entity']);
hide($content['field_implementing_countries']);

hide($content['field_related_links_entity']);
hide($content['field_implementing_agency']);
hide($content['field_donor_enity']);
hide($content['field_project_contact']);
hide($content['field_actions']);
hide($content['field_amount_donor_currency']);


hide($content['field_pifacc_themes']);

hide($content['field_project_sites_3']);



hide($content['field_verified_pccp']);
hide($content['field_project_status']);
hide($content['field_old_drupal_id']);
hide($content['field_drupal_hits']);


//var_dump($content['field_project_status'])


?>
<!-- </pre> -->


<div class="container">

  <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    
  <div class="col-md-8 content-container">

        <!-- Start Article Header -->
        <?php if ($title_prefix || $title_suffix || $display_submitted || !$page): ?>
        <header>
          <?php print render($title_prefix); ?>
          
          <?php if (!$page): ?>
            <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
       
          
        </header>

        <?php endif; ?>
        <!-- End Article Header -->

        <div class="pull-right">
        <?php echo print_pdf_insert_link(); ?>
        </div>
        

        <!-- Start Article Content -->


        <div class="content"<?php print $content_attributes; ?>>
          <!-- Status   -->

            <!-- Description   -->
            <?php if (($tags = render($content['field_the_description'])) ): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Description</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_the_description', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
            <?php endif; ?> 

            <!-- Project Record -->
            <table id="data-table" class="table table-striped table-hover ">
                <tbody>
                    <?php if(render($content['field_project_status'])): ?>
                        <tr>
                            <td>Project Status</td>
                            <td><?php print render($content['field_project_status']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_start_date'])): ?>
                        <tr>
                            <td>Start Date</td>
                            <td><?php print render($content['field_start_date']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_end_date'])): ?>
                        <tr>
                            <td>End Date</td>
                            <td><?php print render($content['field_end_date']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_duration'])): ?>
                        <tr>
                            <td>Duration</td>
                            <td><?php print render($content['field_duration']); ?></td>
                        </tr>
                    <?php endif; ?>


                    <?php if(render($content['field_short_title'])): ?>
                        <tr>
                            <td>Short Title</td>
                            <td><?php print render($content['field_short_title']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_project_type'])): ?>
                        <tr>
                            <td>Project Type</td>
                            <td><?php print render($content['field_project_type']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_project_total_funding'])): ?>
                        <tr>
                            <td>Total Funding</td>
                            <td><?php print render($content['field_project_total_funding']); ?></td>
                        </tr>
                    <?php endif; ?>   

                    <?php if(render($content['field_amount_donor_currency'])): ?>
                        <tr>
                            <td>Donor Currency</td>
                            <td><?php print render($content['field_amount_donor_currency']); ?></td>
                        </tr>
                    <?php endif; ?>    

                    

                    <?php if(render($content['field_project_scope'])): ?>
                        <tr>
                            <td>Project Scope</td>
                            <td><?php print render($content['field_project_scope']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_objectives'])): ?>
                        <tr>
                            <td>Project Objectives</td>
                            <td><?php print render($content['field_objectives']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_actions'])): ?>
                        <tr>
                            <td>Project Actions</td>
                            <td><?php print render($content['field_actions']); ?></td>
                        </tr>
                    <?php endif; ?>


                  

                    

                    <?php if(render($content['field_outputs'])): ?>
                        <tr>
                            <td>Project Outputs</td>
                            <td><?php print render($content['field_outputs']); ?></td>
                        </tr>
                    <?php endif; ?>


                    <?php if(render($content['field_activities'])): ?>
                        <tr>
                            <td>Project Activities</td>
                            <td><?php print render($content['field_activities']); ?></td>
                        </tr>
                    <?php endif; ?>



                </tbody>
            </table>

          <?php
           
            print render($content);
          ?>
        </div>


        
         <!-- field_pifacc_themes   -->
        <?php if (($tags = render($content['field_pifacc_themes'])) ): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">PIFACC Themes</h3>
            </div>
            <div class="panel-body">
                <?php 
                $field = field_view_field('node', $node, 'field_pifacc_themes', array('label'=>'hidden'));
                print render($field) 
                ?>
            </div>
        </div>
        <?php endif; ?> 

         <!-- field_project_sites_3   -->
        <?php if (($tags = render($content['field_project_sites_3'])) ): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Project Sites</h3>
            </div>
            <div class="panel-body">
                <?php 
                //$field = field_view_field('node', $node, 'field_project_sites_3', array('label'=>'hidden'));
                print render($content['field_project_sites_3']);
                ?>
            </div>
        </div>
        <?php endif; ?> 
          
          
        <!-- End Article Content -->





        <!-- Start Article Footer -->
        <footer>

          <?php if ($display_submitted): ?>
            <div class="submitted">
              <?php print $user_picture; ?>
              <span class="glyphicon glyphicon-calendar"></span> <?php print $submitted; ?>
            </div>
          <?php endif; ?>
          </footer>
          <!-- End Article Footer -->

    </div>

    <!-- Start sidebar -->
    <div class="col-md-4 sidebar-container">

         <!-- Implementing Countries -->
        <?php if (($tags = render($content['field_implementing_countries'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Implementing Countries</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_implementing_countries', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_implementing_countries']['#items']) ):
                    $related = $content['field_implementing_countries']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Related Documents -->
        <?php if (($tags = render($content['field_related_documents_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Documents</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_documents_entity']['#items']) ):
                    $related = $content['field_related_documents_entity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Related Projects -->
        <?php if (($tags = render($content['field_related_projects_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Projects</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_projects_entity']['#items']) ):
                    $related = $content['field_related_projects_entity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 


        <!-- Related Links -->
        <?php if (($tags = render($content['field_related_links_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Links</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_links_entity']['#items']) ):
                    $related = $content['field_related_links_entity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                // print '<pre>';
                                // var_dump($entity);
                                // print '</pre>';
                                $title = $entity['entity']->title;
                                $link = $entity['entity']->field_url['und'][0]['value'];

                                //$link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

         <!-- Donor -->
        <?php if (($tags = render($content['field_donor_enity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Donor</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_donor_enity']['#items']) ):
                    $related = $content['field_donor_enity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

         <!-- Contacts-->
        <?php if (($tags = render($content['field_project_contact'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Project Contact</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_project_contact']['#items']) ):
                    $related = $content['field_project_contact']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

         <!-- Implementing Agency -->
        <?php if (($tags = render($content['field_implementing_agency'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Implementing Agency</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_implementing_agency']['#items']) ):
                    $related = $content['field_implementing_agency']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 
       

       

        <!-- Tags -->
        <?php if (($tags = render($content['field_tags'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Tags</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_tags', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Topics -->
        <?php if (($tags = render($content['field_topics'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Topics</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_topics', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 
       

        <!-- Focus area -->
         <?php if (($tags = render($content['field_focus_area'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Focus Area</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_focus_area', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 



        
       

        
        
        


    </div>
    <!-- end sidebar -->

  </article>

</div> <!-- end main project container -->

