
<!-- <pre> -->
<?php 


//print_r($node); 
//print drupal_get_path_alias('node/46');
//Prepare Document
 // We hide the comments and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);

hide($content['field_topics']);
hide($content['field_focus_area']);
hide($content['field_related_links_entity']);
hide($content['field_related_projects_entity']);
hide($content['field_related_documents_entity']);


hide($content['field_verified_pccp']);
hide($content['field_related_links_entity']);
hide($content['field_related_links']);
hide($content['field_related_corporations']);
hide($content['field_country']);
hide($content['field_logo']);
hide($content['field_related_organisation']);




//var_dump($content['field_project_status'])


?>
<!-- </pre> -->

<div class="container">

  <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    
  <div class="col-md-8 content-container">

        <!-- Start Article Header -->
        <?php if ($title_prefix || $title_suffix || $display_submitted || !$page): ?>
        <header>
          <?php print render($title_prefix); ?>
          
          <?php if (!$page): ?>
            <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
          <?php endif; ?>
          <?php print render($title_suffix); ?>
       
          
        </header>
        <?php endif; ?>
        <!-- End Article Header -->

        

        <!-- Start Article Content -->


        <div class="content"<?php print $content_attributes; ?>>
          <!-- Status   -->

            <!-- Description   -->
            <?php if (($tags = render($content['field_the_description'])) ): ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Description</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_the_description', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
            <?php endif; ?> 

            <!-- Org Record -->
            <table id="data-table" class="table table-striped table-hover ">
                <tbody>
                    <?php if(render($content['field_aid_type'])): ?>
                        <tr>
                            <td>Aid Type</td>
                            <td><?php print render($content['field_aid_type']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_objectives'])): ?>
                        <tr>
                            <td>Objectives</td>
                            <td><?php print render($content['field_objectives']); ?></td>
                        </tr>
                    <?php endif; ?>

                    

                    <?php if(render($content['field_global_funding_allocation_'])): ?>
                        <tr>
                            <td>Global Funding Allocation (Donor Currency)</td>
                            <td><?php print render($content['field_global_funding_allocation_']); ?></td>
                        </tr>
                    <?php endif; ?>

                    <?php if(render($content['field_fund_status'])): ?>
                        <tr>
                            <td>Status</td>
                            <td><?php print render($content['field_fund_status']); ?></td>
                        </tr>
                    <?php endif; ?>


                    <?php if(render($content['field_cofinancing'])): ?>
                        <tr>
                            <td>Co-financing Required?</td>
                            <td><?php print render($content['field_cofinancing']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_financing_modality'])): ?>
                        <tr>
                            <td>Financing Modality</td>
                            <td><?php print render($content['field_financing_modality']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_sectoral_focus'])): ?>
                        <tr>
                            <td>Sectoral Focus</td>
                            <td><?php print render($content['field_sectoral_focus']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_contact_details'])): ?>
                        <tr>
                            <td>Contact Details</td>
                            <td><?php print render($content['field_contact_details']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_website'])): ?>
                        <tr>
                            <td>Website</td>
                            <td><?php print render($content['field_website']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_application_procedure'])): ?>
                        <tr>
                            <td>Application Procedure</td>
                            <td><?php print render($content['field_application_procedure']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_country_foucs_text'])): ?>
                        <tr>
                            <td>Country Foucs</td>
                            <td><?php print render($content['field_country_foucs_text']); ?></td>
                        </tr>
                    <?php endif; ?>

                     <?php if(render($content['field_project_examples'])): ?>
                        <tr>
                            <td>Project Examples</td>
                            <td><?php print render($content['field_project_examples']); ?></td>
                        </tr>
                    <?php endif; ?>

                   
                 


                </tbody>
            </table>


        <!-- Related Funds -->
        <?php if (($tags = render($content['field_related_funds'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Funds</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_funds']['#items']) ):
                    $related = $content['field_related_funds']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

          <?php
           
            print render($content);
          ?>
        </div>

          
          
        <!-- End Article Content -->





        <!-- Start Article Footer -->
        <footer>

          <?php if ($display_submitted): ?>
            <div class="submitted">
              <?php print $user_picture; ?>
              <span class="glyphicon glyphicon-calendar"></span> <?php print $submitted; ?>
            </div>
          <?php endif; ?>
          </footer>
          <!-- End Article Footer -->

    </div>

    <!-- Start sidebar -->
    <div class="col-md-4 sidebar-container">
        <!--  Image -->
          <?php if (($tags = render($content['field_logo'])) ): ?>
            <!-- <div class="panel panel-success"> -->
              <!--   <div class="panel-heading">
                    <h3 class="panel-title">Cover Image</h3>
                </div> -->
                <!-- <div class="panel-body"> -->
                    <?php print render($content['field_logo']); ?>
                <!-- </div> -->
            <!-- </div> -->
        <?php endif; ?> 



         <!-- Links -->
        <?php if (($tags = render($content['field_related_links'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Links</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_related_links']); ?>
                </div>
            </div>
        <?php endif; ?> 



        

         <!-- Funding Org -->
        <?php if (($tags = render($content['field_related_organisation'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Funding Organisation</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_organisation', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_organisation']['#items']) ):
                    $related = $content['field_related_organisation']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Related Countries -->
        <?php if (($tags = render($content['field_country'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Countries</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_country', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_country']['#items']) ):
                    $related = $content['field_country']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Related Documents -->
        <?php if (($tags = render($content['field_related_documents_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Documents</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_documents_entity']['#items']) ):
                    $related = $content['field_related_documents_entity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Related Projects -->
        <?php if (($tags = render($content['field_related_projects_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Projects</h3>
                </div>
                <div class="panel-body">
                    <div class="field-items">
                    <?php 
                    // $field = field_view_field('node', $node, 'field_related_documents_entity', array('label'=>'hidden'));
                    // print render($field) 
                    if ( isset($content['field_related_projects_entity']['#items']) ):
                    $related = $content['field_related_projects_entity']['#items'];
                    if (is_array($related)):
                            foreach ($related as $entity) {
                                //var_dump($entity);
                                $title = $entity['entity']->title;
                                $link = drupal_get_path_alias('/node/'.$entity['target_id']);

                                print '<div class="field-item"><a href="'.$link.'">'.$title.'</a></div>';
                            }
                        endif;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        <?php endif; ?> 


       
       

       
        <!-- Tags -->
        <?php if (($tags = render($content['field_tags'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Tags</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_tags', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Topics -->
        <?php if (($tags = render($content['field_topics'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Topics</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_topics', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 
       

        <!-- Focus area -->
         <?php if (($tags = render($content['field_focus_area'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Focus Area</h3>
                </div>
                <div class="panel-body">
                    <?php 
                    $field = field_view_field('node', $node, 'field_focus_area', array('label'=>'hidden'));
                    print render($field) 
                    ?>
                </div>
            </div>
        <?php endif; ?> 



        
       

        
        
        


    </div>
    <!-- end sidebar -->

  </article>

</div> <!-- end main project container -->

