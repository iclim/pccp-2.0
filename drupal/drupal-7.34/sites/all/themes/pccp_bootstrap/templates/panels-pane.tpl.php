<?php
/**
 * @file panels-pane.tpl.php
 * Main panel pane template
 *
 * Variables available:
 * - $pane->type: the content type inside this pane
 * - $pane->subtype: The subtype, if applicable. If a view it will be the
 *   view name; if a node it will be the nid, etc.
 * - $title: The title of the content
 * - $content: The actual content
 * - $links: Any links associated with the content
 * - $more: An optional 'more' link (destination only)
 * - $admin_links: Administrative links associated with the content
 * - $feeds: Any feed icons or associated with the content
 * - $display: The complete panels display object containing all kinds of
 *   data including the contexts and all of the other panes being displayed.
 */
// print '<pre>';
//  print_r($pane->css['css_id']);
//  print '</pre>';
//print $pane->type . "-" .$pane->subtype;
$collapsein = 'in';

if(isset($pane->css)):
    if(isset($pane->css['css_id'])):
        if($pane->css['css_id'] == 'pifacc_themes_facet'):
            $collapsein = '';
        endif;
    endif;
endif;
?>




<?php if ( (strpos($pane->subtype, 'facetapi') !== false) ): ?>
  

      <?php if ($pane_prefix): ?>
        <?php print $pane_prefix; ?>
      <?php endif; ?>
      <div class="panel panel-default panel-facet <?php print $classes; ?>" <?php print $id; ?> <?php print $attributes; ?>>
        <?php if ($admin_links): ?>
          <?php print $admin_links; ?>
        <?php endif; ?>

        <div class="panel-heading">
            <a data-toggle="collapse" data-target="#panel_id_<?php print $pane->pid; ?>" href="#panel_id_<?php print $pane->pid; ?>">
                <?php print $title; ?>
            </a>
        </div>
        


        <div id="panel_id_<?php print $pane->pid; ?>" class="panel-body collapse <?php echo $collapsein; ?>">

            <?php if ($feeds): ?>
              <div class="feed">
                <?php print $feeds; ?>
              </div>
            <?php endif; ?>

           
              <?php print render($content); ?>
           

            <?php if ($links): ?>
              <div class="links">
                <?php print $links; ?>
              </div>
            <?php endif; ?>

            <?php if ($more): ?>
              <div class="more-link">
                <?php print $more; ?>
              </div>
            <?php endif; ?>
          </div>
          <?php if ($pane_suffix): ?>
            <?php print $pane_suffix; ?>
          <?php endif; ?>
      </div>


<?php else: ?>

     <?php if ($pane_prefix): ?>
    <?php print $pane_prefix; ?>
  <?php endif; ?>
  <div class="<?php print $classes; ?>" <?php print $id; ?> <?php print $attributes; ?>>
    <?php if ($admin_links): ?>
      <?php print $admin_links; ?>
    <?php endif; ?>

    <?php print render($title_prefix); ?>
    <?php if ($title): ?>
      <<?php print $title_heading; ?><?php print $title_attributes; ?>>
        <?php print $title; ?>
      </<?php print $title_heading; ?>>
    <?php endif; ?>
    <?php print render($title_suffix); ?>

    <?php if ($feeds): ?>
      <div class="feed">
        <?php print $feeds; ?>
      </div>
    <?php endif; ?>

    <div class="pane-content">
      <?php print render($content); ?>
    </div>

    <?php if ($links): ?>
      <div class="links">
        <?php print $links; ?>
      </div>
    <?php endif; ?>

    <?php if ($more): ?>
      <div class="more-link">
        <?php print $more; ?>
      </div>
    <?php endif; ?>
  </div>
  <?php if ($pane_suffix): ?>
    <?php print $pane_suffix; ?>
  <?php endif; ?>

<?php endif; ?>
