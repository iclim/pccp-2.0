
<!-- <pre> -->
<?php 


//print_r($node); 
//print drupal_get_path_alias('node/46');
//Prepare Document
 // We hide the comments and links now so that we can render them later.
hide($content['comments']);
hide($content['links']);
hide($content['field_tags']);

hide($content['body']);
hide($content['field_related_documents_entity']);
hide($content['field_related_projects_entity']);
hide($content['field_country']);
hide($content['field_organisation_entity']);
hide($content['field_related_news']);


//var_dump($content['field_project_status'])


?>
<!-- </pre> -->

<div class="container">

  <article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
    
  <div class="col-md-8 content-container">
  	<!-- Description   -->
    <?php if (($tags = render($content['body'])) ): ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Description</h3>
        </div>
        <div class="panel-body">
            <?php 
            $field = field_view_field('node', $node, 'body', array('label'=>'hidden'));
            print render($field) 
            ?>
        </div>
    </div>
    <?php endif; ?> 
    <?php print render($content);?>
  </div>



          
          
   





   

    <!-- Start sidebar -->
    <div class="col-md-4 sidebar-container">
        <!-- Countries -->
        <?php if (($tags = render($content['field_country'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Countries</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_country']); ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Documents -->
        <?php if (($tags = render($content['field_related_documents_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Documents</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_related_documents_entity']); ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Projects -->
        <?php if (($tags = render($content['field_related_projects_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Projects</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_related_projects_entity']); ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- Orgs -->
        <?php if (($tags = render($content['field_organisation_entity'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related Organisations</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_organisation_entity']); ?>
                </div>
            </div>
        <?php endif; ?> 

        <!-- News -->
        <?php if (($tags = render($content['field_related_news'])) ): ?>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Related News</h3>
                </div>
                <div class="panel-body">
                    <?php print render($content['field_related_news']); ?>
                </div>
            </div>
        <?php endif; ?> 
</div>
    <!-- end sidebar -->
  <!-- Start Article Footer -->
    <footer>

      <?php if ($display_submitted): ?>
        <div class="submitted">
          <?php print $user_picture; ?>
          <span class="glyphicon glyphicon-calendar"></span> <?php print $submitted; ?>
        </div>
      <?php endif; ?>
      </footer>
      <!-- End Article Footer -->

    </div>
  
</article>
</div> <!-- end main  container -->

