
<?php
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see bootstrap_preprocess_page()
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see bootstrap_process_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */

// print '<pre>';
// var_dump($page['content']['system_main']['nodes'][57]['field_flag_image']);
// print '</pre>';

$navmenu =  menu_tree_all_data('main-menu');
?>

<header id="header" role="banner" class="clearfix navbar container navbar-default">
    <div class="container">

        <!-- #header-inside -->
        <div id="header-inside" class="clearfix">
            <div class="row">
                <div class="col-md-12">
                  <img class="responsive banner-image"  src="/<?php echo path_to_theme(); ?>/images/pccp-banner.png" alt="banner image" />
                </div>
            </div>
        </div>


    </div>
</header>

<header id="navbar" role="banner" class="<?php print $navbar_classes; ?>">
  <div class="container">
    <div class="navbar-header">
    

      <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <?php if (!empty($primary_nav) || !empty($secondary_nav) || !empty($page['navigation'])): ?>
      <div class="navbar-collapse collapse">
        <nav role="navigation">
            <ul class="menu nav navbar-nav">
                  <?php foreach($navmenu as $menu_item): ?>
                    <?php if ( !$menu_item['link']['hidden'] ): ?>
                        
                      <?php if ( !count($menu_item['below']) ): ?>
                        <?php if ( $menu_item['link']['link_title'] === 'Home'): ?>
                            <li class="leaf first"><a href="/"><?php print $menu_item['link']['link_title']; ?></a></li>
                        <?php else: ?>    
                            <?php //if ( $menu_item['link']['link_title'] === 'Regional Search'  || $menu_item['link']['link_title'] === 'RTSM'): ?>
                            <?php if ( strpos($menu_item['link']['link_path'], 'http:') !== false): ?>  
                              <li class="leaf"><a href="<?php print $menu_item['link']['link_path']; ?>"><?php print $menu_item['link']['link_title']; ?></a></li>
                            <?php else: ?>
                              <li class="leaf"><a href="/<?php print $menu_item['link']['link_path']; ?>"><?php print $menu_item['link']['link_title']; ?></a></li>
                            <?php endif; ?>
                        <?php endif; ?>   
                      <?php else: ?>
                        <li class="expanded active-trail dropdown active"><a href="/<?php print $menu_item['link']['link_path']; ?>" title="" class="active-trail dropdown-toggle active"><?php print $menu_item['link']['link_title']; ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <?php foreach($menu_item['below'] as $menu_item_below): ?>
                        <?php if ( strpos($menu_item_below['link']['link_path'], 'http:') !== false): ?>  
                        <li class="leaf first"><a href="<?php print $menu_item_below['link']['link_path']; ?>"><?php print $menu_item_below['link']['link_title']; ?></a></li>
                        <?php else: ?>
                          <li class="leaf first"><a href="/<?php print $menu_item_below['link']['link_path']; ?>"><?php print $menu_item_below['link']['link_title']; ?></a></li>
                        <?php endif; ?>
                      <?php endforeach; ?>
                        <!-- <li class="first leaf"><a href="/country/cook-island">Cook Island</a></li>
                        <li class="leaf"><a href="/country/fsm">FSM</a></li>
                        <li class="leaf"><a href="/country/northern-marianas">Northern Marianas</a></li>
                        <li class="last leaf"><a href="/country/wallis-and-futuna">Wallis and Futuna</a></li> -->
                    </ul>
                </li>
                      <?php endif; ?>
                    <?php endif; ?> 
                  <?php endforeach; ?>
                   
                </ul>
                <div class="region region-navigation">
                    <div>
                        <div class="block-inner clearfix">
                            
                            <div>

                                <form class="form-search content-search" action="/sitesearch" method="get" id="search-block-form" accept-charset="UTF-8">
                                    <div>
                                        <div>
                                            
                                            <div class="input-group">
                                                <span class="sitesearch">
                                                  <input title="Site search" placeholder="Site Search" class="form-control form-text" type="text" id="edit-search-block-form--2" name="search_api_views_fulltext" value="" size="20" maxlength="128">
                                                </span>
                                               <!-- <span class="input-group-btn"><button type="submit" class="btn btn-default"><i class="icon glyphicon glyphicon-search" aria-hidden="true"></i></button> </span> -->
                                            </div>
                                               <!-- <button class="element-invisible btn btn-primary form-submit" id="edit-submit" name="op" value="Search" type="submit">Search</button> -->
                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
          <?php if (!empty($page['navigation'])): ?>
            <?php //print render($page['navigation']); ?>
          <?php endif; ?>
        </nav>
      </div>
    <?php endif; ?>
  </div>
</header>

<div id="main-container" class="main-container container">


  <div class="row">

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section<?php print $content_column_class; ?>>
      <?php if (!empty($page['highlighted'])): ?>
        <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
      <?php endif; ?>
      <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)): ?>
        <h1 class="page-header"><?php print $title; ?>
          <?php if (($tags = render($page['content']['field_flag_image'])) ): ?>  
              <?php print render($page['content']['field_flag_image']); ?>
          <?php endif; ?> 

        </h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php if (!empty($action_links)): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>

<div class="container">
	<div class="pccp-sections" data-example-id="thumbnails-with-content">
   <div class="row white">
<!-- Featured sections -->
<div class="container">
  <div class="pccp-sections" data-example-id="thumbnails-with-content">
    
    <div class="large-icons">

      

      <div class="col-sm-6 col-md-3">


        <a href="/documents">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-documents.jpg" alt="documents" />
        </a>

        <div class="front-page-panel">
          
            <h4>Documents</h4>
            <p>Search for case studies, adaption plans and more.</p>
            <a href="/documents" class="btn btn-orange">Search Now</a>
          
        </div>
      </div>

      <div class="col-sm-6 col-md-3">

        <a href="/projects">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-projects.jpg" alt="projects" />
        </a>

        <div class="front-page-panel">
           <h4>Projects</h4>
           <p>Find out more about projects in your area.</p>
           <a href="/projects" class="btn btn-orange">Find Projects</a>
        </div>
      </div>

      <div class="col-sm-6 col-md-3">
        <a href="/countries">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-countries.jpg" alt="countries" />
        </a>

        <div class="front-page-panel">

            <h4>Country Profiles</h4>
            <p>Detailed Pacific region data and profiles.</p>
            <a href="/countries" class="btn btn-orange">Browse Profiles</a>
 
        </div>
      </div>


      <div class="col-sm-6 col-md-3">

        <a href="/donor-database">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-donor.png" alt="donors" />
        </a>

        <div class="front-page-panel">

            <h4>Donor Database</h4>
            <p>Find out more about donors in the area.</p>
            <a href="/donor-database" class="btn btn-orange">Browse Donors</a>

        </div>

      </div>
     
     </div>
    

    
  </div> <!-- end pccp-sections -->
</div> <!-- end container -->
</div> <!-- end row -->


<div class="row white">
<!-- Featured sections -->
<div class="container">
  <div class="pccp-sections" data-example-id="thumbnails-with-content">
    
    <div class="large-icons">

      

      <div class="col-sm-6 col-md-3">


        <a href="/crop-agency-list">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-list.jpg" alt="crop agency list" />
        </a>

        <div class="front-page-panel">
          
            <h4>CROP Agency List</h4>
            <p>See a registry of CROP agencies.</p>
            <a href="/crop-agency-list" class="btn btn-orange">See Now</a>
          
        </div>
      </div>

      <div class="col-sm-6 col-md-3">

        <a href="/climate-tools">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-tools.jpg" alt="climate-tools" />
        </a>

        <div class="front-page-panel">
           <h4>Climate Tools</h4>
           <p>See a list of Climate Tools relevant to the pacific region.</p>
           <a href="/climate-tools" class="btn btn-orange">Find Tools</a>
        </div>
      </div>

      <div class="col-sm-6 col-md-3">
        <a href="/gallery">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-media.jpg" alt="media" />
        </a>

        <div class="front-page-panel">

            <h4>Media Gallery</h4>
            <p>A collection of relevant media galleries used on the portal.</p>
            <a href="/gallery" class="btn btn-orange">Browse Gallery</a>
 
        </div>
      </div>


      <div class="col-sm-6 col-md-3">

        <a href="/educational-resource-search">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-eduresources.png" alt="educational-resource-search" />
        </a>

        <div class="front-page-panel">

            <h4>Educational Resources</h4>
            <p>Browse the collection of relevant educational resources relating to Climate change and the pacific region.</p>
            <a href="/educational-resource-search" class="btn btn-orange">Browse Resources</a>

        </div>

      </div>
     
     </div>
    

    
  </div> <!-- end pccp-sections -->
</div> <!-- end container -->
</div> <!-- end row -->




 
  </div> <!-- end pccp-sections -->

</div>

<div class="container partners white">
  <div class="col-md-2">
  </div>
  <div class="col-md-8">
    <!-- <h3 class="text-center">Our Partners</h3> -->
    <img class="img-responsive"  src="/<?php echo path_to_theme(); ?>/images/partner_logo_small1.png" alt="partner logos" />
  </div>
  <div class="col-md-2">
  </div>


</div>
<footer class="footer container">
  <p class="text-center">Copyright  © 2015 - Pacific Climate Change Portal, Secretariat of the Pacific Regional Environment Programme (SPREP)</p>
</footer>
