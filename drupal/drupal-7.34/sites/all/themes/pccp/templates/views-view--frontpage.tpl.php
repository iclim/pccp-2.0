<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>



<!-- Carousel-->

<div class="row grey motif-right">
  <div class="container">
  

    <div id="myCarousel" class="carousel slide centered" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
        <li data-target="#myCarousel" data-slide-to="3"></li>
        <li data-target="#myCarousel" data-slide-to="4"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          
          
          <div class="container">
            <img  class="first-slide front-slide-image"  src="<?php echo path_to_theme(); ?>/images/front-1.jpg" alt="First slide" />
            <div class="carousel-caption">
             <!--  <h1>Example headline.</h1>
              <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p> -->
            </div>
          </div>
        </div>


        <div class="item">
          
          <div class="container">
          <img class="second-slide front-slide-image"  src="<?php echo path_to_theme(); ?>/images/front-2.jpg" alt="Second slide" />
            <div class="carousel-caption">
              <!-- <h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> -->
            </div>
          </div>
        </div>


        <div class="item">
          <div class="container">
          <img class="third-slide front-slide-image"  src="<?php echo path_to_theme(); ?>/images/front-3.jpg" alt="Third slide" />
            <div class="carousel-caption">
              <!-- <h1>One more for good measure.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p> -->
            </div>
          </div>
        </div>

        <div class="item">
          
          <div class="container">
          <img class="fourth-slide front-slide-image"  src="<?php echo path_to_theme(); ?>/images/front-5.jpg" alt="Fourth slide" />
            <div class="carousel-caption">
              <!-- <h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> -->
            </div>
          </div>
        </div>

        <div class="item">
          
          <div class="container">
          <img class="fifth-slide front-slide-image"  src="<?php echo path_to_theme(); ?>/images/front-7.jpg" alt="Fifth slide" />
            <div class="carousel-caption">
              <!-- <h1>Another example headline.</h1>
              <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p> -->
            </div>
          </div> 
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->
    
  </div> <!-- /.container -->
</div> <!-- /.row -->

<div class="row white">
<!-- Featured sections -->
<div class="container">
  <div class="pccp-sections" data-example-id="thumbnails-with-content">
    
    <div class="large-icons">

      <h2 style="text-align:center;">Pacific climate resources at your finger tips ... </h2>

      <div class="col-sm-6 col-md-3">


        <a href="/documents">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-documents.jpg" alt="documents" />
        </a>

        <div class="front-page-panel">
          
            <h4>Documents</h4>
            <p>Search for case studies, adaption plans and more.</p>
            <a href="/documents" class="btn btn-orange">Search Now</a>
          
        </div>
      </div>

      <div class="col-sm-6 col-md-3">

        <a href="/projects">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-projects.jpg" alt="projects" />
        </a>

        <div class="front-page-panel">
           <h4>Projects</h4>
           <p>Find out more about projects in your area.</p>
           <a href="/projects" class="btn btn-orange">Find Projects</a>
        </div>
      </div>

      <div class="col-sm-6 col-md-3">
        <a href="/countries">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-countries.jpg" alt="countries" />
        </a>

        <div class="front-page-panel">

            <h4>Country Profiles</h4>
            <p>Detailed Pacific region data and profiles.</p>
            <a href="/countries" class="btn btn-orange">Browse Profiles</a>
 
        </div>
      </div>


      <div class="col-sm-6 col-md-3">

        <a href="/donor-database">
          <img class="img-responsive center-block front-icons"  src="<?php echo path_to_theme(); ?>/images/icon-donor.png" alt="donors" />
        </a>

        <div class="front-page-panel">

            <h4>Donor Database</h4>
            <p>Find out more about donors in the area.</p>
            <a href="/donor-database" class="btn btn-orange">Browse Donors</a>

        </div>

      </div>
     
     </div>
    

    
  </div> <!-- end pccp-sections -->
</div> <!-- end container -->
</div> <!-- end row -->
<div class="row white front-page-search">
   <div class="container">
      <h2 class="page-header">Regional Search</h2>
      <div class="jumbotron col-md-12 grey">
          
          <div class="col-md-7 col-sm-12">
          

            <form class="form-horizontal"  action="/region-search" method="get" id="front-page-region-search-form" accept-charset="UTF-8">
               
                <p>
                  The regional search interface allows you to search for climate information resources in the Pacific region across a range of sources.
                </p>
              <div class="input-group form-group-lg col-sm-10">
                 <input class="form-control" type="text" id="fed-search" class="typeahead" autocomplete="off" placeholder="Enter search keywords ..."  name="textsearch" >
                 <span class="input-group-btn">
                      <button class="btn btn-primary btn-lg" type="submit">Go!</button>
                 </span>
              </div>
            </form>
          </div>
          <div class="col-md-1 col-sm-12"></div>
          <div class="col-md-4 col-sm-12">
            <p>Or, explore projects via the Map Interface:</p>
              
              <a href="/region-search/map.php" class="btn btn-primary" role="button">
                <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Open Map
              </a>

          </div>
      </div>
    </div>
  </div>


  <div class="row grey motif-left">
    <div class="container">
      <div class="col-sm-1">
      </div>

      <div class="col-sm-10">
          <div class="col-sm-8">
            <h3 class="">Recent Updates</h3>
          </div>
          <div class="col-sm-8">
              <div class="panel panel-default panel-front">
                    <div class="panel-body">
                          <ul class="nav nav-tabs">
                                  <li class="active">
                                      <a href="#events" data-toggle="tab">Upcoming Events</a>
                                  </li>
                                  <li>
                                      <a href="#news" data-toggle="tab">Latest News</a>
                                  </li>
                                  <li class="disabled">
                                      <a href="#announcements" data-toggle="tab">Announcements</a>
                                  </li>
                                
                              </ul>
                              <div id="myTabContent" class="tab-content">
                                <div class="tab-pane fade active in" id="events">
                                   <?php
                                  echo views_embed_view('recent_events', 'block');
                                  ?>
                                </div>
                                <div class="tab-pane fade" id="news">
                                  <?php
                                  echo views_embed_view('recent_news', 'block');
                                  ?>
                                </div>
                                <div class="tab-pane fade" id="announcements">
                                  Announcements
                                </div>
                              </div>
                    </div>
              </div>
          </div>
          <div class="col-sm-4">
              <div class="row">
                  <div class="panel panel-default panel-side-small">
                    <div class="panel-body">
                      <h4>New Documents</h4>
                      <?php echo views_embed_view('recent_documents', 'block'); ?>
                      <a href="\documents" class="btn btn-primary btn-lg">See All Documents</a>
                    </div>
                  </div>
              </div>
              <div class="row">
                  <div class="panel panel-default panel-side-small">
                    <div class="panel-body">
                      <h4>New Projects</h4>
                      <?php echo views_embed_view('recent_projects', 'block'); ?>
                      <a href="\projects" class="btn btn-primary btn-lg">See All Projects</a>
                    </div>
                  </div>
              </div>
          </div>

      </div>

      <div class="col-sm-1">
      </div>

        
      </div>



  </div>




